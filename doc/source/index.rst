Life-cycle Manager and Kubernetes Nodes
=======================================

The set of options defined in this schema are shared by the life-cycle manager
and the Kubernetes nodes. The stack element is specific to the
life-cycle manager.

.. jsonschema:: schema.yaml
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target: