#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import os
from os import path
import re
import requests
import shutil
import subprocess
import sys
import tempfile

from cloudinit import subp
from cloudinit import templater

from kanod_configure import common
from kanod_configure import configure

ADMIN_KUBECONFIG = '/etc/kubernetes/admin.conf'

def get_k8s_pause_container():
    pause_container=""
    ret=subp.subp(['kubeadm','config','images','list'])
    containers=ret[0]
    for container in containers.split('\n') : 
        if "k8s.gcr.io/pause:" in container:
            pause_container=container

    return pause_container


def local_pause_container(conf, file):
    nexus = conf.get('nexus', {})
    registry = nexus.get('docker', None)
    pause_container = get_k8s_pause_container()

    command = [
        'sed', '-i',
        f's!"k8s.gcr.io/pause:.*"!"{registry}/{pause_container}"!', file]
    subp.subp(command)


def get_containerd_auth(conf):
    '''get authentication tokens for private registries'''
    containers_vars = conf.get('containers', {})
    raw_auths = containers_vars.get('auths', None)
    if raw_auths is None:
        return None
    auths = [
        {
            "repository": cell.get('repository', ''),
            "username": cell.get('username', ''),
            "password": cell.get('password', ''),
        }
        for cell in raw_auths
    ]
    return auths


def container_engine_containerd_config(conf):
    '''Configure the containerd container engine'''
    proxy_vars = conf.get('proxy')
    if proxy_vars is not None:
        configure.render_template(
            'container_engine_proxy.tmpl',
            'etc/systemd/system/containerd.service.d/http-proxy.conf',
            proxy_vars
        )
    insecure_registries = configure.get_insecure_registries(conf)
    mirrors = conf.get('containers', {}).get('registry_mirrors', None)
    auths = get_containerd_auth(conf)
    configure.render_template(
        'containerd_registries.tmpl',
        'etc/containerd/config.d/registries.toml',
        {'insecures': insecure_registries, 'mirrors': mirrors, 'auths': auths},
        resource_package=__name__)
    configure.render_template(
        'containerd_kubelet.tmpl',
        'etc/default/kubelet',
        {},
        resource_package=__name__
    )
    configure.render_template(
        'crictl.tmpl',
        'etc/crictl.yaml',
        {'socket': '/var/run/containerd/containerd.sock'},
        resource_package=__name__)
    local_pause_container(conf, '/etc/containerd/config.toml')
    subp.subp(['systemctl', 'daemon-reload'])
    subp.subp(['systemctl', 'enable', 'containerd'])
    subp.subp(['systemctl', 'start', 'containerd'])


def container_engine_crio_config(conf):
    '''Configure the crio container engine'''
    proxy_vars = conf.get('proxy')
    if proxy_vars is not None:
        configure.render_template(
            'container_engine_proxy.tmpl',
            'etc/systemd/system/crio.service.d/http-proxy.conf',
            proxy_vars
        )
    insecure_registries = configure.get_insecure_registries(conf)
    mirrors = conf.get('containers', {}).get('registry_mirrors', [])
    configure.render_template(
        'crio_registries.tmpl',
        'etc/containers/registries.conf.d/50-kanod.conf',
        {'insecures': insecure_registries,
         'mirrors': mirrors},
        resource_package=__name__
    )
    configure.render_template(
        'crio_kubelet.tmpl',
        'etc/default/kubelet',
        {},
        resource_package=__name__
    )
    configure.render_template(
        'crictl.tmpl',
        'etc/crictl.yaml',
        {'socket': '/var/run/crio/crio.sock'},
        resource_package=__name__)
    local_pause_container(conf, '/etc/crio/crio.conf')
    subp.subp(['systemctl', 'daemon-reload'])
    subp.subp(['systemctl', 'enable', 'crio'])
    subp.subp(['systemctl', 'start', 'crio'])


def k8s_postconfig(conf):
    '''Configure kubernetes (post kubeadm)'''
    export_kubeconfig(conf)
    if conf.get('kubernetes', {}).get('untaint', False) :
        print('remove taint on control plane nodes')
        untaint()


def configure_calico(conf):
    print('configure calico')
    k8s_vars = conf.get('kubernetes', {})
    version = k8s_vars.get('calico', None)
    if version is None:
        return
    nexus = conf.get('nexus', {})
    maven = nexus.get('maven', None)
    registry = nexus.get('docker', None)
    pod_cidr = k8s_vars.get('pod-network-cidr', '192.168.192.0/18')
    ip_autodetection_method = k8s_vars.get('ip_autodetection_method', None)
    if maven is None or registry is None:
        return
    certificate = nexus.get('certificate', None)
    if certificate is None:
        verify = True
    else:
        with tempfile.NamedTemporaryFile('w', delete=False) as fd:
            verify = fd.name
            fd.write(certificate)
            fd.write('\n')
    with tempfile.TemporaryDirectory() as temp_dir:
        url = (
            '{0}/repository/kanod/kanod/calico/{1}/'
            'calico-{1}.yaml').format(maven, version)
        req = requests.get(url, allow_redirects=True, verify=verify)
        relocated = templater.basic_render(
            req.content.decode('utf-8'),
            {'NEXUS_REGISTRY': registry})
        with open(path.join(temp_dir, 'calico.yaml'), 'wb') as file:
            file.write(relocated.encode('utf-8'))
        if isinstance(verify, str):
            os.remove(verify)
        configure.render_template(
            'fix_pod_cidr.tmpl',
            path.join(temp_dir, 'fix_pod_cidr.yaml'),
            {
                'pod_cidr': pod_cidr,
                'ip_autodetection_method': ip_autodetection_method
            },
            resource_package=__name__
        )
        configure.render_template(
            'kustomization_calico.tmpl',
            path.join(temp_dir, 'kustomization.yaml'),
            {},
            resource_package=__name__
        )
        command = [
            'kubectl', '--kubeconfig', ADMIN_KUBECONFIG, 'apply',
            '-k', temp_dir
        ]
        subp.subp(command)


def export_kubeconfig(conf):
    print('export kubeconfig')
    k8s_vars = conf.get('kubernetes', {})
    role = k8s_vars.get('role', None)
    if role is None:
        return
    if role in ['control', 'mononode']:
        target = path.join(configure.ROOT, 'home/admin/kubeconfig')
        shutil.copyfile(ADMIN_KUBECONFIG, target)
        shutil.chown(target, user='admin')


def launch_kubeadm(system, k8s_vars, nexus_registry, container_engine):
    print('launch kubeadm')
    target_ip = k8s_vars.get('endpoint')
    pod_cidr = k8s_vars.get('pod-network-cidr', '192.168.192.0/18')
    k8s_version = system.get('k8s_version', 'v1.18.8')
    command = [
        'kubeadm', 'init', '--apiserver-advertise-address', target_ip,
        '--pod-network-cidr={}'.format(pod_cidr),
        '--kubernetes-version', k8s_version,
    ]
    if nexus_registry is not None:
        command += [
            '--image-repository',
            '{}/k8s.gcr.io'.format(nexus_registry)
        ]
    if container_engine == "containerd":
        command += [
            "--cri-socket", "unix:///run/containerd/containerd.sock"
        ]
    if container_engine == "crio":
        command += [
            "--cri-socket", "unix:///var/run/crio/crio.sock"
        ]
    proc = subprocess.run(command, stdout=sys.stdout, stderr=subprocess.STDOUT)
    if proc.returncode != 0:
        print('kubernetes launch failed')
        raise Exception('kubeadm failed')


def untaint():
    '''Taint the node so that pods can be deployed on masters'''
    command = [
        'kubectl', '--kubeconfig', ADMIN_KUBECONFIG,
        'taint', 'nodes', '--all', 'node-role.kubernetes.io/master-'
    ]
    subp.subp(command)


def launch_keepalived(endpoint, keepalived):
    k8s_itf = keepalived.get('interface', None)
    router_id = keepalived.get('router_id', None)
    if router_id is None:
        address_re = re.compile(r'[0-9]*\.[0-9]*\.[0-9]*\.([0-9]*)')
        match = address_re.match(endpoint)
        if match is not None:
            router_id = match.group(1)
        else:
            router_id = '2'
    configure.render_template(
        'keepalived.tmpl', 'etc/keepalived/keepalived.conf',
        {'k8s_itf': k8s_itf, 'endpoint': endpoint, 'router_id': router_id},
        resource_package=__name__
    )
    try:
        requests.get('https://{}:6443/healthz'.format(endpoint), timeout=10, verify=False)
    except:    # noqa: disable=E722
        command = ['systemctl', 'start', 'keepalived']
        subp.subp(command)
    command = ['systemctl', 'enable', '--now', 'monitor-keepalived.service']
    subp.subp(command)


def vault_register(config, vault_save):
    '''Register Kubernetes in vault

    This token is consumed by the updaters to create the approle tokens
    for launched clusters and by argocd for manifest specialization.
    '''
    if vault_save.get('token', None) is None:
        return
    k8s_cfg = config.get('kubernetes', {})
    address = k8s_cfg.get('endpoint', '')
    vault_name = k8s_cfg.get('vault_name', 'lcm')
    environ = {
        'KUBECONFIG': '/etc/kubernetes/admin.conf',
        'VAULT_URL': vault_save['url'],
        'LCM_TOKEN': vault_save['token'],
        'VAULT_CA': vault_save['verify'],
        'ADDRESS': address,
        'PORT': '6443',
        'VAULT_NAME': vault_name
    }
    process = subprocess.run(
        ['/usr/local/bin/bind-vault'], env=environ,
        stdout=sys.stdout, stderr=subprocess.STDOUT)
    # print(process.stdout.decode('utf-8'))
    if process.returncode == 0:
        print('Kubernetes cluster bound to Vault')
    else:
        print('Vault binding failed')


def k8s_config(system, conf, vault_save):
    '''Configure kubernetes (pre kubadm)'''
    k8s_vars = conf.get('kubernetes', {})
    role = k8s_vars.get('role', None)
    if role is None:
        return
    container_engine = k8s_vars.get('container_engine', 'crio')
    nexus = conf.get('nexus', None)
    nexus_registry = nexus.get('docker', None) if nexus is not None else None
    if container_engine == 'docker':
        print('docker')
        configure.container_engine_docker_config(conf)
    elif container_engine == 'containerd':
        print('containerd')
        container_engine_containerd_config(conf)
    elif container_engine == 'crio':
        print('crio')
        container_engine_crio_config(conf)
    else:
        raise Exception('unknown container engine')
    subp.subp(['systemctl', 'enable', 'kubelet'])
    keepalived = k8s_vars.get('keepalived', None)
    if keepalived is not None:
        launch_keepalived(k8s_vars.get('endpoint'), keepalived)

    if role == 'mononode':
        launch_kubeadm(system, k8s_vars, nexus_registry, container_engine)
        print('Post-config')
        k8s_postconfig(conf)
        # configure calico on our own for mononode
        configure_calico(conf)
        # Taint nodes for master
        untaint()
        # Register Vault (not too early)
        vault_register(conf, vault_save)


def augment_network_ironic(conf):
    print('Specific addons to network for ironic')
    state = conf.get('network')
    k8s_vars = conf.get('kubernetes', {})
    pxe_itf = k8s_vars.get('pxe_itf', None)
    if pxe_itf is not None and k8s_vars.get('role', '') == 'control':
        bridges = state.setdefault('bridges', [])
        bridges['ironicendpoint'] = {'interfaces': [pxe_itf], 'dhcp4': True}


def configure_k8s_node(init, system, conf):
    vault_save = {}

    def vault_callback(config, vault_url, vault_token, verify):
        vault_save['url'] = vault_url
        vault_save['token'] = vault_token
        vault_save['verify'] = verify

    augment_network_ironic(conf)
    configure.configure_base(
        init, system, conf, vault_callback=vault_callback)
    print('Kubernetes config')
    k8s_config(system, conf, vault_save)


def init():
    configure.register('node', configure_k8s_node)


def main_post():
    (_, _, conf, _) = configure.initialize()
    k8s_postconfig(conf)
